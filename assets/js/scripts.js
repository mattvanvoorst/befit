$(document).ready(function(){
    setTimeout(function(){
        $('.outer_white_circle').removeClass('pre_animation');
    }, 500);

    setTimeout(function(){
        $('.circle_trio_container').removeClass('pre_animation');
    }, 800);

    setTimeout(function(){
        $('.cogwheel_container').removeClass('pre_animation');
    }, 1050);

    setTimeout(function(){
        $('.outer_green_circle').removeClass('pre_animation');
    }, 1200);

    setTimeout(function(){
        $('.dashed_ring').removeClass('pre_animation');
    }, 1200);
});